module.exports = {
  transpileDependencies: ["vuetify"],

  devServer: {
    proxy: require("./src/config").apiBaseUrl,
  },
};
