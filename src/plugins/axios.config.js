import axios from "axios";
import config from "../config";

if (!config.proxy) {
  axios.defaults.baseURL = config.apiBaseUrl;
}

axios.interceptors.response.use(
  (response) => {
    return response;
  },
  (error) => {
    const err = error.response.request.responseURL;

    console.error(err);

    throw error;
  }
);

export default axios;
