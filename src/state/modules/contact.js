import axios from "axios";

export const state = {};

export const getters = {};

export const mutations = {};

export const actions = {
  async sendMessge({ commit }, params) {
    const { name, email, message } = params;

    return await axios.post(`/contact`, {
      firstAndLastName: name,
      phone: "000000000",
      email,
      topic: "FROM LANDING PAGE 4EMOTIVO",
      message,
    });
  },
};
