export const state = {
  show: false,
  description: "",
  type: "",
};

export const getters = {
  getIsVisible: (state) => {
    return state.show;
  },

  getDescription: (state) => {
    return state.description;
  },

  getAlertType: (state) => {
    return state.type;
  },
};

export const mutations = {
  SET_ALERT(state, value) {
    state.show = value.show;
    state.description = value.description;
    state.type = value.type ? value.type : "success";
  },
};

export const actions = {};
