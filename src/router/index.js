import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

const lazyLoadView = (AsyncView) => {
  const AsyncHandler = () => ({
    component: AsyncView,
    delay: 400,
    timeout: 10000,
  });

  return Promise.resolve({
    functional: true,
    render(h, { data, children }) {
      return h(AsyncHandler, data, children);
    },
  });
};

const routes = [
  {
    path: "/",
    name: "Home",
    component: () => lazyLoadView(import("../views/Home.vue")),
    meta: {
      layout: "default",
    },
  },
  {
    path: "/contact",
    name: "Contact",
    component: () => lazyLoadView(import("../views/Contact.vue")),
    meta: {
      layout: "default",
    },
  },

  {
    path: "/*",
    name: "error",
    component: () => lazyLoadView(import("../views/NotFound")),
    meta: {
      layout: "default",
    },
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
