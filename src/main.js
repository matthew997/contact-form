import Vue from "vue";
import App from "./App.vue";

import vuetify from "./plugins/vuetify";
import "./plugins/vuelidate";
import "./plugins/axios.config";

import router from "./router";
import store from "./state/store";

import "./registerServiceWorker";

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  vuetify,
  render: (h) => h(App),
}).$mount("#app");
