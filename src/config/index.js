require("dotenv");

module.exports = {
  apiBaseUrl: process.env.VUE_APP_API_BASE_URL,
  publicPath: "/",
  proxy: process.env.VUE_APP_USE_PROXY ? true : false,
};
